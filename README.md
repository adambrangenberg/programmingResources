# Programming Resources

## Description

This project allows you to quickly find resources for specific topics in programming. 
The project is now live at [resources.tigerbyte.dev](https://resources.tigerbyte.dev/). 

If you have questions about why it's running on a subdomain, go monorepo that way or something different, check out the [FAQ](https://gitlab.com/adambrangenberg/programmingResources/-/wikis/FAQ) :3

## Contributing
You can check out our [Bucket List](https://gitlab.com/adambrangenberg/programmingResources/-/boards/4828988) to see what is on the list right now or add something you'd love to see (here please a new issue first :^) ).
To see how and what you can contribute check out our [Tutorial on Contribution](https://gitlab.com/adambrangenberg/programmingResources/-/wikis/How-to-Contribute).

You can also open a new [issue](https://gitlab.com/adambrangenberg/programmingResources/-/issues) to report things or suggest new Features, Languages, etc.

## License
This project is licensed under the copyleft General Public License 3.0. Read more [here](https://www.gnu.org/licenses/gpl-3.0.de.html)

## Contact
You can either open a new [issue](https://gitlab.com/adambrangenberg/programmingResources/-/issues) in that repository 
or use the [contact page](https://tigerbyte.dev/contact) on my homepage
