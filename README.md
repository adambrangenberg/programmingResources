# Programming Resources

## Description

This project allows you to quickly find resources for specific topics in programming. 
The project is now live at [resources.tigerbyte.dev](https://resources.tigerbyte.dev/).

## Contributing
You can check out our [Projects](https://codeberg.org/adambrangenberg/programmingResources/projects) to see what is on the list right now or add something you'd love to see (here please a new issue first :^) ).
To see how and what you can contribute check out our [Tutorial on Contribution](https://codeberg.org/adambrangenberg/programmingResources/wiki/How-to-Contribute).

You can also open a new [issue](https://codeberg.org/adambrangenberg/programmingResources/issues) to report things or suggest new Features, Languages, etc.

## License
This project is licensed under the copyleft General Public License 3.0. Read more [here](https://www.gnu.org/licenses/gpl-3.0.de.html)

## Contact
You can either open a new [issue](https://codeberg.org/adambrangenberg/programmingResources/issues) in that repository 
or use the [contact page](https://tigerbyte.dev/contact) on my homepage
