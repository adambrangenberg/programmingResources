import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  searchString = "";

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  // @ts-ignore
  async searchInput({ target, keyCode }) {
    if (keyCode === 13) {
      await this.router.navigate([`render/${this.searchString}`]);
    }
    this.searchString = target.value;
  }
}
