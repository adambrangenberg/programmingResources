export interface resourceLink {
  title: string;
  resource: string;
  description: string[];
  publisher: String;
  language: String;
}

export interface ressourceData {
  title: string;
  resources: resourceLink[];
}
