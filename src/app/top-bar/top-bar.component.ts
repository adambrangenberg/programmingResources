import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  searchString = "";
  language = "en";
  languages = new Map<string, string>([
      ["en", "🇬🇧"],
      ["de", "🇩🇪"]
    ]
  );
  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  // @ts-ignore
  async searchInput({target, keyCode}) {
    if (keyCode === 13) {
      await this.router.navigate([`render/${this.searchString}`]);
    }
    this.searchString = target.value;
  }
}
