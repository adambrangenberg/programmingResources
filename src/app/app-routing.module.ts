import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainPageComponent} from "./main-page/main-page.component";
import {RenderRessourcesComponent} from "./render-ressources/render-ressources.component";

const routes: Routes = [
  {
    path: "",
    component: MainPageComponent
  },
  {
    path: "render/:file",
    component: RenderRessourcesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
