import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainPageComponent} from "./main-page/main-page.component";

const routes: Routes = [
  {
    path: "",
    component: MainPageComponent
  },
  {
    path: ":file",
    component: MainPageComponent
  },

  // Keeping support for old paths for now...
  {
    path: "render/:file",
    component: MainPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
