import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-main-page',
  template: `
    <app-render-ressources
      [request]="name"
    ></app-render-ressources>
  `
})
export class MainPageComponent implements OnInit {
  public name: String = "random";

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params?.subscribe(params => {
      const file = params["file"]
      this.name = typeof file === "string" ? "topic/" + file : this.name;
    });
  }
}
