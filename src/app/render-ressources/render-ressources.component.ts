import {Component, Input, OnInit} from '@angular/core';
import {resourceLink, ressourceData} from "../types/resources";


@Component({
  selector: 'app-render-ressources',
  templateUrl: './render-ressources.component.html',
  styleUrls: ['./render-ressources.component.scss']
})
export class RenderRessourcesComponent implements OnInit {
  @Input() request!: String;

  displayName = "";
  links: resourceLink[] = [];

  status = 200
  info = "We couldn't seem to find this. Please try it with another search term or contact us about this with the link in the top. We'd appreciate it if you would contribute content."

  constructor() {
  }

  async ngOnInit(): Promise<void> {
    const testing = true
    const path = testing ? "https://resources.tigerbyte.dev" : location.origin
    try {
      const response = await fetch(`${path}/api/${this.request}`,
        {
          headers: {
            "Access-Control-Allow-Origin": "*",
          },
          method: "GET"
        })
      this.status = response?.status
      const data: ressourceData = await response?.json()

      this.displayName = data.title;
      this.links = data.resources ?? [];
    } catch(error) {
      this.displayName = "The server didn't respond.";
    }
  }
}
