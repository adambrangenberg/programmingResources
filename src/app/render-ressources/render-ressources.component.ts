import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

interface resourceLink {
  displayName: string;
  url: string;
  description: string[];
}

interface ressourceData {
  displayName: string;
  links: resourceLink[];
}

@Component({
  selector: 'app-render-ressources',
  templateUrl: './render-ressources.component.html',
  styleUrls: ['./render-ressources.component.scss']
})
export class RenderRessourcesComponent implements OnInit {
  displayName = "";
  links: resourceLink[] = [];

  status = 200
  info = "We couldn't seem to find this. Please try it with another search term or contact us about this with the link in the top. We'd appreciate it if you would contribute content."

  constructor(private route: ActivatedRoute) {
  }

  async ngOnInit(): Promise<void> {
    let link;
    this.route.params.subscribe(params => {
      link = params["file"];
    });

    const response = await fetch(`${location.origin}/api/topic/${link}`,
      {
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
        method: "GET"
      })

    this.status = response?.status
    const data = await response?.json()

    this.displayName = data.displayName;
    this.links = data.links ?? [];
  }
}
