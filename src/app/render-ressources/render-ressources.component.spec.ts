import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenderRessourcesComponent } from './render-ressources.component';

describe('RenderRessourcesComponent', () => {
  let component: RenderRessourcesComponent;
  let fixture: ComponentFixture<RenderRessourcesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenderRessourcesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RenderRessourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
