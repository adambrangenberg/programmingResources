import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { MainPageComponent } from './main-page/main-page.component';
import { RenderRessourcesComponent } from './render-ressources/render-ressources.component';
import { RessourceCardComponent } from './ressource-card/ressource-card.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchBarComponent,
    MainPageComponent,
    RenderRessourcesComponent,
    RessourceCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
