import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TopBarComponent} from './top-bar/top-bar.component';
import {MainPageComponent} from './main-page/main-page.component';
import {RenderRessourcesComponent} from './render-ressources/render-ressources.component';
import {RessourceCardComponent} from './ressource-card/ressource-card.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    MainPageComponent,
    RenderRessourcesComponent,
    RessourceCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
