import {Component, Input, OnInit} from '@angular/core';

interface resourceLink {
  title: string;
  resource: string;
  description: string[];
  publisher: String;
  language: String;
}

@Component({
  selector: 'app-ressource-card',
  templateUrl: './ressource-card.component.html',
  styleUrls: ['./ressource-card.component.scss']
})
export class RessourceCardComponent implements OnInit {
  @Input() link!: resourceLink;
  @Input() number!: number;

  numberFormatter(numberToFormat: number) {
    if (numberToFormat + 1 < 10) {
      return "0" + (numberToFormat + 1);
    } else {
      return numberToFormat + 1;
    }
  }
  constructor() {
  }

  ngOnInit(): void {
  }

}
