package dev.tigerbyte.plugins;

import io.ktor.client.plugins.websocket.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.server.testing.*
import kotlin.test.Test
import kotlin.test.assertEquals

class RoutingKtTest {

  @Test
  fun testGetApiTopic() = testApplication {
    application {
      configureRouting()
    }
    client.get("/api/topic/kotln").apply {
      assertEquals(HttpStatusCode.OK, status)
      val content = this.javaClass.classLoader.getResource("topics/kotlin.json")?.readText()
      assertEquals(content, bodyAsText())
    }

    client.get("/api/topic//Ich%20mag%20kartoffelpuffer").apply {
      assertEquals(HttpStatusCode.NotFound, status)
      val content = "{ \"displayName\": \"404\" }"
      assertEquals(content, bodyAsText())
    }
  }
}
