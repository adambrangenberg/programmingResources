package dev.tigerbyte

import io.ktor.server.routing.*
import io.ktor.http.*
import io.ktor.server.plugins.cors.routing.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.request.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlin.test.*
import io.ktor.server.testing.*
import dev.tigerbyte.plugins.*

class ApplicationTest {
  @Test
  fun testRoot() = testApplication {
    application {
      configureRouting()
    }
    client.get("/api/topic").apply {
      assertEquals(HttpStatusCode.NotFound, status)
      assertEquals("{ \"displayName\": \"Please define a topic\" }", bodyAsText())
    }
  }
}

