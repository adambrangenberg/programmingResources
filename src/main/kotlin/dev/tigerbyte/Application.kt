package dev.tigerbyte

import dev.kord.core.Kord
import dev.kord.core.event.interaction.ButtonInteractionCreateEvent
import dev.kord.core.event.interaction.ModalSubmitInteractionCreateEvent
import dev.kord.core.on
import dev.tigerbyte.discord.Handler
import dev.tigerbyte.plugins.*
import io.ktor.serialization.gson.*
import io.ktor.server.application.*
import io.ktor.server.cio.*
import io.ktor.server.engine.*
import io.ktor.server.plugins.contentnegotiation.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

suspend fun main() {
  coroutineScope {
    launch {
      startBot()
    }
    launch {
      startAPI(8080, "0.0.0.0")
    }
  }
}

fun startAPI(port: Int, host: String) {
  embeddedServer(CIO, port = port, host = host) {
    configureHTTP()
    configureRouting()
    install(ContentNegotiation) {
      gson {
        setPrettyPrinting()
      }
    }
  }.start(wait = true)
}

suspend fun startBot() {
  val kord = Kord(System.getenv("discordToken"))
  val handler = Handler()

  kord.on<ButtonInteractionCreateEvent> {
    if (interaction.componentId.startsWith("accept")) {
      handler.acceptButton(interaction)
      return@on
    }

    if (interaction.componentId.startsWith("add")) {
      handler.addButton(interaction)
    }
  }

  kord.on<ModalSubmitInteractionCreateEvent> {
    if (interaction.modalId.startsWith("submit")) {
      handler.submitModal(interaction)
    }
  }

  kord.login()
}
