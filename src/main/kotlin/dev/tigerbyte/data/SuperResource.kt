package dev.tigerbyte.data

abstract class SuperResource {
  abstract val language: String
  abstract val resource: String
  abstract val title: String
  abstract val description: Array<String>
}
