package dev.tigerbyte.data

data class ResourceExposedData(
  val publisher: String,
  override val language: String,
  override val resource: String,
  override val title: String,
  override val description: Array<String>
) : SuperResource() {
  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as ResourceExposedData

    if (publisher != other.publisher) return false
    if (language != other.language) return false
    if (resource != other.resource) return false
    if (title != other.title) return false
    if (!description.contentEquals(other.description)) return false

    return true
  }

  override fun hashCode(): Int {
    var result = publisher.hashCode()
    result = 31 * result + language.hashCode()
    result = 31 * result + resource.hashCode()
    result = 31 * result + title.hashCode()
    result = 31 * result + description.contentHashCode()
    return result
  }
}
