package dev.tigerbyte.data

data class ResourceResponseData(
  val time: String,
  val status: String,
  val result: Array<ResourceData>
) {
  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as ResourceResponseData

    if (time != other.time) return false
    if (status != other.status) return false
    if (!result.contentEquals(other.result)) return false

    return true
  }

  override fun hashCode(): Int {
    var result1 = time.hashCode()
    result1 = 31 * result1 + status.hashCode()
    result1 = 31 * result1 + result.contentHashCode()
    return result1
  }
}
