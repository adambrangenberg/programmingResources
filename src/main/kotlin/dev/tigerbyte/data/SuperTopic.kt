package dev.tigerbyte.data

abstract class SuperTopic {
  abstract val topic: String
  abstract val title: String
  abstract val related: Array<String>
}
