package dev.tigerbyte.data

public data class TopicPaths(val idk: String) {
  public val mappedPaths: Map<String, String>
    get() = mapOf(
        "c++" to "cpp",
        "cpp" to "cpp",

        "java" to "java",

        "javascript" to "javascript",

        "kotlin" to "kotlin",

        "python" to "python",
        )
}
