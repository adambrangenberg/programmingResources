package dev.tigerbyte.data

data class TopicData(
  val id: String,
  override val topic: String,
  val alias: Array<String>,
  override val related: Array<String>,
  val resources: Array<String>,
  override val title: String
): SuperTopic() {
  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as TopicData

    if (topic != other.topic) return false
    if (!related.contentEquals(other.related)) return false
    if (!resources.contentEquals(other.resources)) return false

    return true
  }

  override fun hashCode(): Int {
    var result = topic.hashCode()
    result = 31 * result + related.contentHashCode()
    result = 31 * result + resources.contentHashCode()
    return result
  }
}
