package dev.tigerbyte.routes

import dev.tigerbyte.data.TopicReceiveData
import dev.tigerbyte.discord.InputValidation
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.*
import io.ktor.server.request.*
import io.ktor.server.routing.*


fun Route.setTopics() {
  val validator = InputValidation()
  post("/topic") {
    val success = try {
      val body = call.receive<TopicReceiveData>()

      validator.topic(body)
    } catch (error: BadRequestException) {
      false
    }

    if (success) {
      call.response.status(HttpStatusCode.Created)
    } else {
      call.response.status(HttpStatusCode.BadRequest)
    }
  }
}
