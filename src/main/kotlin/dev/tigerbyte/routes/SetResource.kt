package dev.tigerbyte.routes

import dev.tigerbyte.data.ResourceReceiveData
import dev.tigerbyte.discord.InputValidation
import dev.tigerbyte.discord.Publisher
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.*
import io.ktor.server.request.*
import io.ktor.server.routing.*

fun Route.setResource() {
  val validator = InputValidation()
  post("/resource") {
    val success = try {
      val body = call.receive<ResourceReceiveData>()
      val publisher = Publisher(body.publisher)

      // Checking if it's a valid Discord User,
      // if not make them Anonym
      body.publisher = if (publisher.isValid()) {
        body.publisher
      } else {
        "Anonym"
      }

      validator.resource(body)
    } catch (error: BadRequestException) {
      false
    }

    if (success) {
      call.response.status(HttpStatusCode.Created)
    } else {
      call.response.status(HttpStatusCode.BadRequest)
    }
  }
}

