package dev.tigerbyte.routes

import com.google.gson.Gson
import dev.tigerbyte.data.ResourceExposedData
import dev.tigerbyte.data.TopicExposedData
import dev.tigerbyte.utilities.Levenshtein
import dev.tigerbyte.utilities.ResourceManager
import dev.tigerbyte.utilities.TopicManager
import dev.tigerbyte.utilities.createTopicReplyData
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlin.random.Random

fun Route.getTopics() {
  val manager = TopicManager()
  val resourcer = ResourceManager()
  val comparer = Levenshtein()

  get("/random") {
    val topics = manager.getTopics()

    if (topics == null) {
      call.response.status(HttpStatusCode.NotFound)
      call.respondText("{ \"displayName\": \"404\" }")
    } else {

      // Making sure that even if there is a error, a reply gets found using recursion
      suspend fun getTopic() {
        val randomNumber = Random.nextInt(topics.size)
        val topic = topics[randomNumber]

        val exposedData = createTopicReplyData(topic)
        if (exposedData == null) {
          getTopic()
        } else {
          call.response.status(HttpStatusCode.OK)
          val jsonTopic = Gson().toJson(exposedData)
          call.respondText(jsonTopic)
        }
      }

      getTopic()
    }
  }

  get("/topic/{name}") {
    val parameters = call.parameters["name"]?.lowercase()!!.split(" ")

    var name = ""
    var highestSimilarity = 0.0
    for (parameter in parameters) {
      val comparision = comparer.checkWithDB(parameter)
      if (comparision.second > highestSimilarity) {
        name = comparision.first
        highestSimilarity = comparision.second
      }
    }

    val topic = manager.getTopic(name) ?: manager.getTopicByAlias(name)
    val exposedData = createTopicReplyData(topic)
    if (exposedData == null) {
      call.response.status(HttpStatusCode.NotFound)
      call.respondText("{ \"displayName\": \"404\" }")
    } else {
      call.response.status(HttpStatusCode.OK)
      val jsonTopic = Gson().toJson(exposedData)
      call.respondText(jsonTopic)
    }
  }


  get("/topic") {
    val topics = manager.getTopics()
    val exposedTopics = mutableListOf<TopicExposedData>()

    // Converting the topics to an exposable one
    topics?.forEach { topic ->
      val exposedResources = mutableListOf<ResourceExposedData>()

      // Fetching and Converting every resource to an exposable one
      topic.resources.forEach { resource ->
        val fetchedResource = resourcer.getResource(resource)

        if (fetchedResource != null) {
          val exposedResource = ResourceExposedData(
            resource = fetchedResource.resource,
            publisher = fetchedResource.publisher,
            language = fetchedResource.language,
            title = fetchedResource.title,
            description = fetchedResource.description
          )

          exposedResources.add(exposedResource)
        }
      }
      println(topic.topic)
      println(topic.title)
      val exposedTopic = TopicExposedData(
        topic = topic.topic,
        related = topic.related,
        resources = exposedResources.toTypedArray(),
        title = topic.title
      )

      exposedTopics.add(exposedTopic)
    }

    call.response.status(HttpStatusCode.OK)
    val jsonTopics = Gson().toJson(exposedTopics)
    call.respondText(jsonTopics)
  }
}
