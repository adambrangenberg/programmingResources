package dev.tigerbyte.devTools

import dev.kord.common.entity.ButtonStyle
import dev.kord.common.entity.Snowflake
import dev.kord.rest.builder.message.create.actionRow
import dev.kord.rest.builder.message.create.embed
import dev.kord.rest.service.RestClient

class Discord {
  private val rest = RestClient(System.getenv("discordToken"))
  suspend fun createSubmitMessage(channelId: Snowflake) {
    rest.channel.createMessage(channelId) {
      content = "Click to add a Topic or Resource \uD83D\uDDC3"

      embed {
        title = "Tutorial"
        field {
          name = "General things"
          value = "Everything that includes the verbs displayed or shown is seen being displayed and are meant to look good. " +
            "For example could you type C++ instead of cpp." +
            "The other things should stay generic because it's only meant for managing purposes."
        }

        field {
          name = "Topics"
          value = "To submit a new Topic using the button you need to ensure following things:\n" +
            "**1.** Ensure that the topic parameter only includes letters and space and is as simple as possible. This is just meant for management.\n" +
            "**2.** The same thing applies to alias and related topics\n" +
            "**3.** That your topic doesn't already exist in some sort"
          inline = true
        }

        field {
          name = "Resources"
          value = "Also to submit a new Resource you need to ensure some things:\n" +
            "**1.** Same as point 1 in topics. It's used to indentify the relation to a topic.\n" +
            "**2.** Ensure the link is valid and target running."
          inline = true
        }
      }

      actionRow {
        interactionButton(ButtonStyle.Primary, "add_topic") {
          label = "Add a Topic \uD83D\uDCDA"
        }

        interactionButton(ButtonStyle.Primary, "add_resource") {
          label = "Add a Resource \uD83D\uDCD5"
        }
      }
    }
  }
}
