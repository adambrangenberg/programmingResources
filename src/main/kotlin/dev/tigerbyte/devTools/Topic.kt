package dev.tigerbyte.devTools

data class Topic(
  val topic: String,
  val title: String,
  val related: Array<String>,
  val alias: Array<String>,
  val resources: Array<Resource>
) {
  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as Topic

    if (topic != other.topic) return false
    if (!related.contentEquals(other.related)) return false
    if (!resources.contentEquals(other.resources)) return false

    return true
  }

  override fun hashCode(): Int {
    var result = topic.hashCode()
    result = 31 * result + related.contentHashCode()
    result = 31 * result + resources.contentHashCode()
    return result
  }
}
