package dev.tigerbyte.devTools

data class Resource(
  val publisher: String,
  val language: String,
  val resource: String,
  val title: String,
  val description: Array<String>
) {
  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as Resource

    if (publisher != other.publisher) return false
    if (language != other.language) return false
    if (resource != other.resource) return false
    if (title != other.title) return false
    if (!description.contentEquals(other.description)) return false

    return true
  }

  override fun hashCode(): Int {
    var result = publisher.hashCode()
    result = 31 * result + language.hashCode()
    result = 31 * result + resource.hashCode()
    result = 31 * result + title.hashCode()
    result = 31 * result + description.contentHashCode()
    return result
  }
}
