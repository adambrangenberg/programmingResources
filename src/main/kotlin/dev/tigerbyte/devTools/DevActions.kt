package dev.tigerbyte.devTools

import com.google.gson.Gson
import dev.tigerbyte.utilities.ResourceManager
import dev.tigerbyte.utilities.TopicManager

class DevActions {
  val topicManager = TopicManager()
  val resourceManager = ResourceManager()
  suspend fun fillDatabase() {
    val topics = arrayOf("apps", "cpp", "java", "javascript", "kotlin", "python")

    topics.forEach { topicName ->
      val file = loadFile(topicName)
      val data = Gson().fromJson(file, Topic::class.java)

      topicManager.addTopic(data.topic, data.title, data.related, data.alias)
      data.resources.forEach { resource ->
        resourceManager.addResource(resource.resource, resource.publisher, resource.language, resource.title, resource.description)
        topicManager.addResourceToTopic(data.topic, resource.resource)
      }
    }
  }

  private fun loadFile(name: String): String? {
    return object {}.javaClass.classLoader.getResource("topics/$name.json")?.readText()
  }
}
