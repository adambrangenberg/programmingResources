package dev.tigerbyte.discord

import dev.kord.common.entity.Snowflake
import dev.kord.rest.request.KtorRequestException
import dev.kord.rest.service.RestClient

class Publisher(id: String) {
  private val userId = Snowflake(id)
  private val rest = RestClient(System.getenv("discordToken"))

  suspend fun isValid(): Boolean {
    return try {
      rest.user.getUser(userId)
      true
    } catch(error: KtorRequestException) {
      false
    }
  }
}
