package dev.tigerbyte.discord

import com.google.gson.Gson
import dev.kord.common.entity.TextInputStyle
import dev.kord.core.behavior.interaction.modal
import  dev.kord.core.behavior.interaction.respondEphemeral
import dev.kord.core.entity.interaction.ButtonInteraction
import dev.kord.core.entity.interaction.ModalSubmitInteraction
import dev.tigerbyte.data.ResourceReceiveData
import dev.tigerbyte.data.TopicReceiveData
import dev.tigerbyte.utilities.ResourceManager
import dev.tigerbyte.utilities.TopicManager
import io.ktor.util.*

class Handler {
  val topicManager = TopicManager()
  val resourceManager = ResourceManager()
  val validator = InputValidation()

  suspend fun acceptButton(button: ButtonInteraction) {
    val messageContent = button.message.content.drop(1).dropLast(1)

    if (button.componentId.endsWith("resource")) {
      val resource = Gson().fromJson(messageContent, ResourceReceiveData::class.java)
      resourceManager.addResource(
        resource.resource,
        resource.publisher,
        resource.language,
        resource.title,
        resource.description
      )
      topicManager.addResourceToTopic(resource.topic, resource.resource)

      button.respondEphemeral {
        content = "Added ${resource.title} to ${resource.topic}! \uD83D\uDE42"
      }

      return
    }

    if (button.componentId.endsWith("topic")) {
      val topic = Gson().fromJson(messageContent, TopicReceiveData::class.java)
      topicManager.addTopic(topic.topic, topic.title, topic.related, topic.alias)

      button.respondEphemeral {
        content = "Added ${topic.topic} to the Database! \uD83D\uDDC3"
      }

      return
    }
  }

  suspend fun addButton(button: ButtonInteraction) {
    if (button.componentId.endsWith("resource")) {
      button.modal("Add a Resource", "submit_resource") {
        actionRow {
          textInput(TextInputStyle.Short, "topic", "To which topic does this resource belong to?") {
            allowedLength = 1..20
          }
        }

        actionRow {
          textInput(TextInputStyle.Short, "resource", "What is the link of the resource?") {
            allowedLength = 11..40
            placeholder = "https://example.com/docs"
          }
        }

        actionRow {
          textInput(TextInputStyle.Short, "title", "How should it be displayed??") {
            allowedLength = 3..20
          }
        }

        actionRow {
          textInput(TextInputStyle.Paragraph, "description", "What description should be shown?") {
            allowedLength = 20..500
          }
        }
      }

      return
    }

    if (button.componentId.endsWith("topic")) {
      button.modal("Add a Topic", "submit_topic") {
        actionRow {
          textInput(TextInputStyle.Short, "topic", "What topic are you trying to add?") {
            allowedLength = 1..20
          }
        }

        actionRow {
          textInput(TextInputStyle.Short, "related", "What topics are related it? Split with ', '") {
            allowedLength = 0..50
            placeholder = "foo, bar, lorem impsum"
            required = false
          }
        }

        actionRow {
          textInput(TextInputStyle.Short, "alias", "Do Exist alias for the topic? Split with ', '") {
            allowedLength = 3..50
            placeholder = "fancy alias, boring alias, way shorter alias"
            required = false
          }
        }

        actionRow {
          textInput(TextInputStyle.Short, "title", "How should the topic be displayed?") {
            allowedLength = 3..20
          }
        }
      }

      return
    }
  }


  suspend fun submitModal(modal: ModalSubmitInteraction) {
    val inputParams = modal.textInputs

    // Checking if topic is valid
    val topicOption = inputParams["topic"]?.value?.toLowerCasePreservingASCIIRules()
    if (topicOption is String && !topicOption.replace(" ", "").onlyLetters()) {
      modal.respondEphemeral {
        content = "Only use letters and spaces are allowed for the topic!"
      }

      return
    }

    if (modal.modalId.endsWith("resource")) {
      if (!validator.isValidURL(inputParams["resource"]?.value!!)) {
        modal.respondEphemeral {
          content = "Invalid URL `${inputParams["resource"]?.value!!}`❌"
        }

        return
      }

      val resource = ResourceReceiveData(
        publisher = modal.user.id.toString(),
        topic = topicOption!!.replace(" ", "-"),
        language = "en",
        resource = inputParams["resource"]?.value!!,
        title = inputParams["title"]?.value!!,
        description = inputParams["description"]?.value!!
          .split("\r?\n|\r".toRegex()).toTypedArray()
          .filter { it != "" }.toTypedArray()
      )
      validator.resource(resource)

      modal.respondEphemeral {
        content = "Submitted for review ✅"
      }

      return
    }

    if (modal.modalId.endsWith("topic")) {
      val topic = TopicReceiveData(
        topic = topicOption!!.replace(" ", "-"),
        alias = inputParams["alias"]?.value
          ?.split(", ".toRegex())
          ?.map {
            it
              .toLowerCasePreservingASCIIRules()
              .replace(" ", "-")
          }
          ?.toTypedArray()
          ?: arrayOf(),
        related = inputParams["related"]?.value
          ?.split(", ".toRegex())
          ?.map {
            it
              .toLowerCasePreservingASCIIRules()
              .replace(" ", "-")
          }
          ?.toTypedArray()
          ?: arrayOf(),
        title = inputParams["title"]?.value!!
      )

      validator.topic(topic)

      /* modal.respondEphemeral {
        content = "Submitted for review ✅"
      }*/

      return
    }
  }

  fun String.onlyLetters() = all { it.isLetter() }
}

