package dev.tigerbyte.discord

import com.google.gson.Gson
import dev.kord.common.entity.ButtonStyle
import dev.kord.common.entity.Snowflake
import dev.kord.rest.builder.message.create.actionRow
import dev.kord.rest.builder.message.create.embed
import dev.kord.rest.request.KtorRequestException
import dev.kord.rest.service.RestClient
import dev.tigerbyte.data.ResourceReceiveData
import dev.tigerbyte.data.TopicReceiveData
import dev.tigerbyte.utilities.Levenshtein
import java.net.URL

class InputValidation {
  private val rest = RestClient(System.getenv("discordToken"))
  private val channelID: Snowflake = Snowflake(System.getenv("submitChannel"))
  private val comparer = Levenshtein()

  suspend fun topic(topic: TopicReceiveData): Boolean {
    val closest = comparer.checkWithDB(topic.topic).first
    println(closest)

    return try {
      rest.channel.createMessage(channelID) {
        content = "`${Gson().toJson(topic)}`"

        embed {
          title = topic.topic
          description = "**Related:** ${topic.related.map { "$it, " }}\n" +
            "**Alias:** ${topic.alias.map { "$it, " }}\n" +
            "**Closest in Database:** $closest"
        }

        actionRow {
          interactionButton(ButtonStyle.Primary, "accept_topic") {
            label = "Accept the Topic"
            disabled = false
          }
        }
      }

      true
    } catch (error: KtorRequestException) {
      false
    }
  }

  suspend fun resource(resource: ResourceReceiveData): Boolean {
    return try {
      rest.channel.createMessage(channelID) {
        content = "`${Gson().toJson(resource)}`"

        embed {
          title = resource.title
          description = resource.description.map { "$it\n\n" }.toString()
          author {
            name = "Language: ${resource.language}"
            url = resource.resource
          }
        }

        actionRow {
          interactionButton(ButtonStyle.Primary, "accept_resource") {
            label = "Accept the Resource"
          }

          linkButton(resource.resource) {
            label = "Go To Website"
          }
        }
      }

      true
    } catch (error: KtorRequestException) {
      false
    }
  }

  fun isValidURL(url: String): Boolean = try {
    URL(url).toURI()
    true
  } catch (error: Exception) {
    false
  }
}

