package dev.tigerbyte.plugins

import dev.tigerbyte.data.TopicPaths
import dev.tigerbyte.utilities.Levenshtein
import io.ktor.http.*
import io.ktor.server.routing.*
import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.response.*

fun Application.configureRouting() {

  routing {
    route("/api") {
      route("/topic") {
        get("{name}") {
          val paths = TopicPaths("idk")
          val parameters = call.parameters["name"]?.lowercase()!!.split(" ")
          val name = paths.mappedPaths[parameters[0]]

          var finalName = paths.mappedPaths[parameters[0]]
          if (name == null) {
            var highestSimilarity = 0.0

            for (parameter in parameters) {
              val comparer = Levenshtein()

              for (key in paths.mappedPaths.keys) {
                val similarity = comparer.findSimilarity(parameter, key)

                if (similarity > highestSimilarity && similarity > 0.4) {
                  highestSimilarity = similarity
                  finalName = key

                  if (similarity == 1.0) {
                    break
                  }
                }
              }
            }
          }

          val file = paths.mappedPaths[finalName]

          if (file == null) {
            call.response.status(HttpStatusCode.NotFound)
            call.respondText("{ \"displayName\": \"404\" }")
          } else {
            val content = this.javaClass.classLoader.getResource("topics/$file.json")?.readText() ?: ""

            call.response.status(HttpStatusCode.OK)
            call.respondText(content)
          }
        }
      }

      get("/topic") {
        call.response.status(HttpStatusCode.NotFound)
        call.respondText("{ \"displayName\": \"Please define a topic\" }")
      }
    }

    singlePageApplication {
      angular("programming-resources")
    }
  }
}
