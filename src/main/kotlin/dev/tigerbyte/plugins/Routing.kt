package dev.tigerbyte.plugins

import dev.tigerbyte.routes.*
import io.ktor.server.routing.*
import io.ktor.server.application.*
import io.ktor.server.http.content.*

fun Application.configureRouting() {
  routing {
    route("/api") {
      getTopics()
      setTopics()
      setResource()
    }

    singlePageApplication {
      angular("programming-resources")
    }
  }
}
