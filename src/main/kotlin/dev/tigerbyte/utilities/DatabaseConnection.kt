package dev.tigerbyte.utilities

import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*

class DatabaseConnection(pUsername: String, pPassword: String, pUrl: String, pDatabase: String, pNamespace: String) {
  private var namespace = pNamespace
  private var database = pDatabase

  private val username = pUsername
  private val password = pPassword
  private val url = pUrl
  private val client = HttpClient()

  // @TODO: Think about a way to write a test for that
  suspend fun request(query: String): HttpResponse {
    return client.post("$url/sql") {
      headers {
        append(HttpHeaders.Accept, "application/json")
        append("NS", namespace)
        append("DB", database)
      }
      setBody(query)
      basicAuth(username, password)
    }
  }

  fun kill() {
    client.close()
  }
}
