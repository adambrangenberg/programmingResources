package dev.tigerbyte.utilities

import com.google.gson.Gson
import dev.tigerbyte.data.TopicData
import dev.tigerbyte.data.TopicResponseData
import io.ktor.client.statement.*
import io.ktor.util.*

@Suppress("NAME_SHADOWING")
class TopicManager {
  private val database = DatabaseConnection(
    System.getenv("dbUsername"),
    System.getenv("dbPassword"),
    System.getenv("dbUrl"),
    System.getenv("dbDatabase"),
    System.getenv("dbNamespace")
  )

  suspend fun addTopic(topic: String, title: String, related: Array<String>, alias: Array<String>): HttpResponse? {
    if (getTopic(topic) !== null) return null

    var formattedRelated = "["
    related.forEach {
      formattedRelated += makeParsable(it) + ", "
    }
    formattedRelated += "]"

    var formattedAlias = "["
    alias.forEach {
      formattedAlias += makeParsable(it) + ", "
    }
    formattedAlias += "]"

    val topic = makeParsable(topic)
    val title = makeParsable(title, false)

    return database.request("CREATE topics SET topic = $topic, related = $formattedRelated, alias = $formattedAlias, title=$title")
  }

  suspend fun addResourceToTopic(topic: String, url: String): HttpResponse {
    val url = makeParsable(url)
    val topicID = getTopic(topic)?.id

    return database.request("UPDATE $topicID SET resources += [$url]")
  }

  suspend fun getTopic(topic: String): TopicData? {
    val topic = makeParsable(topic)

    val request = database.request("SELECT * FROM topics WHERE topic = $topic")

    // Getting the response and removing Array thingies
    val responseText = request.bodyAsText().drop(1).dropLast(1)
    return try {
      responseObject(responseText)?.get(0)
    } catch (error: ArrayIndexOutOfBoundsException) {
      null
    }
  }

  suspend fun getTopicByAlias(alias: String): TopicData? {
    val alias = makeParsable(alias)

    val request = database.request("SELECT * FROM topics WHERE alias CONTAINS  $alias")

    // Getting the response and removing Array thingies
    val responseText = request.bodyAsText().drop(1).dropLast(1)
    return try {
      responseObject(responseText)?.get(0)
    } catch (error: ArrayIndexOutOfBoundsException) {
      null
    }
  }

  suspend fun getTopics(): Array<TopicData>? {
    val request = database.request("SELECT * FROM topics")

    // Getting the response and removing Array thingies
    val responseText = request.bodyAsText().drop(1).dropLast(1)
    return responseObject(responseText)
  }

  suspend fun getTopicNames(): Array<MutableList<String>> {
    val topics = getTopics()
    val names = mutableListOf<String>()
    val aliases = mutableListOf<String>()

    topics?.forEach {
      names.add(it.topic)
      it.alias.forEach { alias ->
          aliases.add(alias)
      }
    }

    return arrayOf(names, aliases)
  }

  private fun responseObject(text: String): Array<TopicData> {
    val response: TopicResponseData = Gson().fromJson(text, TopicResponseData::class.java)
    return response.result
  }

  private fun makeParsable(convert: String, lower: Boolean=true): String {
    return if (lower) {
      "\"$convert\"".toLowerCasePreservingASCIIRules()
    } else {
      "\"$convert\""
    }
  }
}
