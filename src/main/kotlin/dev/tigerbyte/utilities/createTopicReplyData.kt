package dev.tigerbyte.utilities

import dev.tigerbyte.data.ResourceExposedData
import dev.tigerbyte.data.TopicData
import dev.tigerbyte.data.TopicExposedData

suspend fun createTopicReplyData(topic: TopicData?): TopicExposedData? {
  val resourceManager = ResourceManager()

  if (topic?.resources == null) {
    return null
  } else {
    // Filtering unnecessary data out
    val resources = mutableListOf<ResourceExposedData>()
    topic.resources.forEach {
      val resource = resourceManager.getResource(it)
      if (resource != null) {
        resources.add(
          ResourceExposedData(
            resource = resource.resource,
            publisher = resource.publisher,
            language = resource.language,
            title = resource.title,
            description = resource.description
          )
        )
      }
    }

    val exposedData = TopicExposedData(
      topic = topic.topic,
      related = topic.related,
      resources = resources.toTypedArray(),
      title = topic.title
    )

    return exposedData
  }
}
