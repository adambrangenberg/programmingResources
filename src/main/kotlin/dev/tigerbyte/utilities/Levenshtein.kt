package dev.tigerbyte.utilities
import kotlin.math.min
import kotlin.math.max

class Levenshtein {
  private val manager = TopicManager()

  private fun getDistance(initial: String, compare: String): Int {
    val lengthI = initial.length
    val lengthC = compare.length

    val table = Array(lengthI + 1) { IntArray(lengthC + 1) }

    for (row in 1..lengthI) {
      table[row][0] = row
    }
    for (column in 1..lengthC) {
      table[0][column] = column
    }

    for (column in 1..lengthI) {
      for (row in 1..lengthC) {
        // Comparing letter to each other
        // if equal 0, else 1
        val different = if (initial[column - 1] == compare[row - 1]) 0 else 1

        // Comparing all elements in the corne with each other
        // to fill the square
        //
        // 1 2
        // 4

        // 4 + 1 = 5; 2 + 1 = 3
        // 5 > 3 --> 3 in next comparison
        // 1 + isDifferent (If things are different, it'll add 1) = (1 or 2)
        // (1 or 2) < 3 --> (1 or 2) wins

        // 1 2
        // 4 (1 or 2)
        // ⇛ and so on, until the right-bottom corner is reached
        table[column][row] = min(min(
            table[column - 1][row] + 1, table[column][row - 1] + 1),
          table[column - 1][row - 1] + different
        )
      }
    }

    // And when finished, the element in the right-bottom corner is the distance
    return table[lengthI][lengthC]
  }

  fun findSimilarity(initial: String, compare: String): Double {
    val maxLength = max(initial.length, compare.length)

    return if (maxLength > 0) {
      (maxLength * 1.0 - getDistance(initial, compare)) / maxLength * 1.0
    } else 1.0
  }

  suspend fun checkWithDB(parameter: String): Pair<String, Double> {
    val paths = manager.getTopicNames()
    var name = ""
    var highestSimilarity = 0.0

    pathsLoop@for (list in paths) {
      for (key in list) {
        val similarity = findSimilarity(parameter, key)

        if (similarity > highestSimilarity && similarity > 0.4) {
          highestSimilarity = similarity
          name = key

          if (similarity == 1.0) {
            break@pathsLoop
          }
        }
      }
    }

    return Pair(first = name, second = highestSimilarity)
  }
}
