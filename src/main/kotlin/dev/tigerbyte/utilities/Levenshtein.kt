package dev.tigerbyte.utilities
import kotlin.math.min
import kotlin.math.max

class Levenshtein {
  private fun getDistance(initial: String, compare: String): Int {
    val lenghtI = initial.length
    val lenghtC = compare.length

    val table = Array(lenghtI + 1) { IntArray(lenghtC + 1) }

    for (row in 1..lenghtI) {
      table[row][0] = row
    }
    for (column in 1..lenghtC) {
      table[0][column] = column
    }

    for (column in 1..lenghtI) {
      for (row in 1..lenghtC) {
        // Buchstaben miteinander vergleichen
        // wenn gleich 0, sonst 1
        val different = if (initial[column - 1] == compare[row - 1]) 0 else 1

        // Es werden immer die anderen Elemente aus der
        // Tabellenecke verglichen
        //
        // 1 2
        // 4

        // 4 + 1 = 5; 2 + 1 = 3
        // 5 > 3 --> 3 in engere Auswahl
        // 1 + (Summand, wenn Sachen unterschiedlich) = 1 oder 2
        // 1 oder 2 < 3 --> 1 oder 2 gewinnt

        // 1 2
        // 4 1 oder 2
        // ⇛ usw bis zur letzten Ecke unten rechts
        table[column][row] = min(min(
            table[column - 1][row] + 1, table[column][row - 1] + 1),
          table[column - 1][row - 1] + different
        )
      }
    }

    // Element unten Rechts ist dann die Distanz
    return table[lenghtI][lenghtC]
  }

  public fun findSimilarity(initial: String, compare: String): Double {
    val maxLength = max(initial.length, compare.length)

    return if (maxLength > 0) {
      (maxLength * 1.0 - getDistance(initial, compare)) / maxLength * 1.0
    } else 1.0
  }
}
