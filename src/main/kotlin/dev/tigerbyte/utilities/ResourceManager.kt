package dev.tigerbyte.utilities

import com.google.gson.Gson
import dev.tigerbyte.data.ResourceData
import dev.tigerbyte.data.ResourceResponseData
import io.ktor.client.statement.*
import io.ktor.util.*

@Suppress("NAME_SHADOWING")
class ResourceManager {
  private val database = DatabaseConnection(
    System.getenv("dbUsername"),
    System.getenv("dbPassword"),
    System.getenv("dbUrl"),
    System.getenv("dbDatabase"),
    System.getenv("dbNamespace")
  )

  suspend fun addResource(resource: String, publisher: String, language: String, title: String, description: Array<String>): HttpResponse? {
    if (getResource(resource) !== null) return null

    // @TODO Using receive object for that later
    val resource = makeParsable(resource)
    val publisher = makeParsable(publisher)
    val language = makeParsable("en")// makeParsable(language)
    val title = makeParsable(title, false)

    var formattedDescription = "["
    description.forEach {
      formattedDescription += makeParsable(it, false) + ", "
    }
    formattedDescription += "]"

    return database.request(
      "CREATE resources SET " +
      "resource = $resource, " +
      "publisher = $publisher, " +
      "language = $language, " +
      "title=$title, " +
      "description=$formattedDescription"
    )
  }

  suspend fun getResource(resource: String): ResourceData? {
    val resource = makeParsable(resource)

    val request = database.request("SELECT * FROM resources WHERE resource = $resource")

    // Getting the response and removing Array thingies
    val responseText = request.bodyAsText().drop(1).dropLast(1)
    return try {
      val response: ResourceResponseData = Gson().fromJson(responseText, ResourceResponseData::class.java)
      response.result[0]
    } catch (error: ArrayIndexOutOfBoundsException) {
      null
    }
  }
  private fun makeParsable(convert: String, lower: Boolean=true): String {
    return if (lower) {
      "\"$convert\"".toLowerCasePreservingASCIIRules()
    } else {
      "\"$convert\""
    }  }
}


