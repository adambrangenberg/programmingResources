FROM gradle:7-jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle buildFatJar --no-daemon

FROM node:16.14.0-alpine AS buildNode
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build

FROM openjdk:11
EXPOSE 8080:8080
RUN mkdir /app
COPY --from=build /home/gradle/src/build/libs/backend-all.jar /backend-all.jar
COPY --from=buildNode /usr/src/app/dist/programming-resources/ /programming-resources/
RUN ls /
RUN ls /programming-resources
ENTRYPOINT ["java","-jar","/backend-all.jar"]
