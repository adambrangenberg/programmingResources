## Adding a Resource

**[This will be changed soon](https://gitlab.com/adambrangenberg/programmingResources/-/issues/14)**

~~You can add a resource by navigating to the [src/main/resources/topics](https://codeberg.org/adambrangenberg/programmingResources/src/branch/main/src/main/resources/topics) folder. There you can add a new JSON file with the topic or edit an existing one.
If you add a new JSON file make sure it's setup in [src/main/kotlin/dev/tigerbyte/data/TopicPaths.kt](https://codeberg.org/adambrangenberg/programmingResources/src/branch/main/src/main/kotlin/dev/tigerbyte/data/TopicPaths.kt). This will make sure that you can also use aliases like c++ to cpp and that the file is being recognized by the Levenshtein algorithm.~~


## Reporting Issues or Suggesting Features

It's also helpful to just report issues so that we are aware of them and suggest new ideas to help improve the project. 

## Improving the Source Code

This Project uses Angular with TypeScript and KTor with Kotlin. If you know those languages and have some spare time but don't know what to add, it'd be great if you could help with improving the site or implementing things from the [Projects Board](https://gitlab.com/adambrangenberg/programmingResources/-/boards/)
